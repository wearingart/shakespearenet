import { ShakespearenetPage } from './app.po';

describe('shakespearenet App', function() {
  let page: ShakespearenetPage;

  beforeEach(() => {
    page = new ShakespearenetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('snet works!');
  });
});
