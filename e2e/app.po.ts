import { browser, element, by } from 'protractor';

export class ShakespearenetPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('snet-root h1')).getText();
  }
}
