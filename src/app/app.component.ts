import { Component } from '@angular/core';

@Component({
  selector: 'snet-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    backToTop() {
        window.scroll(0,0);
    }
}
