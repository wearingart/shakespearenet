import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { PlaysComponent } from './plays/plays.component';
import { HeaderComponent } from './header.component';
import { HomeComponent } from './home.component';
import { routing } from "./app.routing";
import { PlayItemComponent } from './plays/play-item.component';
import { MacbethComponent } from './plays/macbeth/macbeth.component';
import { JuliuscaesarComponent } from './plays/juliuscaesar/juliuscaesar.component';
import { MuchadoComponent } from './plays/muchado/muchado.component';
import { RomeojulietComponent } from './plays/romeojuliet/romeojuliet.component';

@NgModule({
  declarations: [
    AppComponent,
    PlaysComponent,
    HeaderComponent,
    HomeComponent,
    PlayItemComponent,
    MacbethComponent,
    JuliuscaesarComponent,
    MuchadoComponent,
    RomeojulietComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
