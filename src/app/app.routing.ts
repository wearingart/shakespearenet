import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./home.component";
import {PlaysComponent} from "./plays/plays.component";
import {PlayItemComponent} from "./plays/play-item.component";
import {MacbethComponent} from "./plays/macbeth/macbeth.component";
import {JuliuscaesarComponent} from "./plays/juliuscaesar/juliuscaesar.component";
import {MuchadoComponent} from "./plays/muchado/muchado.component";
import {RomeojulietComponent} from "./plays/romeojuliet/romeojuliet.component";

const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'plays', component: PlaysComponent },
    { path: 'plays/macbeth', component: MacbethComponent },
    { path: 'plays/juliuscaesar', component: JuliuscaesarComponent },
    { path: 'plays/muchado', component: MuchadoComponent},
    { path: 'plays/romeojuliet', component: RomeojulietComponent},
    { path: '**', component: HomeComponent}
];

export const routing = RouterModule.forRoot(APP_ROUTES);