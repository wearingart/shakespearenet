import { Component, OnInit, Input } from '@angular/core';
import { Play } from './play';

@Component({
  selector: 'snet-play-item',
  templateUrl: './play-item.component.html',
  styleUrls: ['./play-item.component.css']
})
export class PlayItemComponent implements OnInit {
  @Input() play: Play;

  constructor() { }

  ngOnInit() {
  }

}
