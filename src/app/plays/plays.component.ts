import { Component, OnInit } from '@angular/core';
import {Play} from "./play";

@Component({
  selector: 'snet-plays',
  templateUrl: './plays.component.html',
  styleUrls: ['./plays.component.css']
})

export class PlaysComponent implements OnInit {
    plays: Play[] = [
        new Play('Julius Caesar', 'juliuscaesar', 'Shakespeare\'s tragedy about the assassination of Julius Caesar and the events that took place after his death.'),
        new Play('Macbeth', 'macbeth', 'Shakespeare\'s tragedy about the great hero, Macbeth, who allows ambition to overtake him and ultimately destroy him.'),
        new Play('Much Ado About Nothing', 'muchado', 'Shakespeare\'s comedy where Benedick and Beatrice are tricked into confessing their love for each other, and Claudio is tricked into rejecting Hero at the altar.'),
        new Play('Romeo and Juliet', 'romeojuliet', 'Shakespeare\'s tragedy about two young lovers from two warring families. Their untimely deaths ultimately brings peace to both families.')
    ];

  constructor() { }

  ngOnInit() {

  }

  private log(hook: string) {
      console.log(hook);
  }
}
